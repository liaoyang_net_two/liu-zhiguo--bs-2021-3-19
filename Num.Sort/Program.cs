﻿using System;

namespace Num.Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            //建立一个名字为shuzu的数组
            int[] shuzu = new int[] { 3, 5, 7, 1, 2, 4, 6, 8, 9 };
            //定义了一个变量，他的值是0
            int bian = 0;
            //定义了一个for循环，用于确定循环多少次，循环次数为数组长度-1
            for(int i=0;i<shuzu.Length-1;i++)
            {
                //嵌套for循环，用于锁定最大的值
                for(int j=0;j<shuzu.Length-(i+1);j++)
                    //定义了一个if语句，用于比较数组索引的大小
                    if(shuzu[j]>shuzu[j+1])
                    {
                        //把数组里j+1的索引值赋给变量bian
                        bian = shuzu[j + 1];
                        //把数组里j的索引值赋给数组里j+1
                        shuzu[j+1] = shuzu[j];
                        //把变量bian的值赋给数组j+1
                        shuzu[j] = bian;
                    }
            }
            //定义了一个foreach循环，用于打印数组
            foreach (var item in shuzu)
            {
                Console.WriteLine($"{item}\t");
            }
        }
    }
}
